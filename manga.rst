Salut
=========

Voici un **texte en gras**

.. glossary::

   zoro

      .. image:: ./_static/zoro.jpeg

      Roronoa Zoro, Zoro ou anciennement francisé en Roronoa Zorro, est un personnage de One Piece. 
      Il s'agit d'un épéiste dont l'ambition est de devenir le meilleur épéiste du monde.

   luffy

      .. image:: ./_static/luffy.jpg
         
      Monkey D. Luffy, surnommé de manière récurrente Luffy au chapeau de paille, est un personnage de fiction et principal protagoniste de la franchise japonaise One Piece.
